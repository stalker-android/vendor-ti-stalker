#!/system/bin/sh

# insert cmemk with params for DM37x (720P video) first
# If fails it is assumed the device is OMAP35x (480P video)
insmod /system/ti-dsp/cmemk.ko "phys_start=0x84700000 phys_end=0x85900000 allowOverlap=1 useHeapIfPoolUnavailable=1" ||
insmod /system/ti-dsp/cmemk.ko "phys_start=0x86b00000 phys_end=0x87200000 allowOverlap=1 useHeapIfPoolUnavailable=1"

insmod /system/ti-dsp/dsplinkk.ko
insmod /system/ti-dsp/lpm_omap3530.ko
insmod /system/ti-dsp/sdmak.ko

sleep 1
chmod 0666 /dev/cmem
chmod 0666 /dev/dsplink
chmod 0666 /dev/lpm0
chmod 0666 /dev/sdma

stop media
start media
