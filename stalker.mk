PRODUCT_PACKAGES := \
	ApiDemos      \
        SoundRecorder \
        GeneralRunner \
        EvilSkull \
        Vase

$(call inherit-product, build/target/product/generic.mk)

# Overrides
PRODUCT_BRAND := TI
PRODUCT_NAME := omap3evm
PRODUCT_DEVICE := omap3evm
PRODUCT_PACKAGE_OVERLAYS := device/rowboat/generic
# Install the features available on this device.
PRODUCT_COPY_FILES += \
    frameworks/base/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml